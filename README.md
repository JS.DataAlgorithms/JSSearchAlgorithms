JavaScript Search Algorithms
=============


1. Binary Search: binarySearch<br/>
2. Sequential Search: sequentialSearch<br/>


<b>Usage:</b><br/>
```
import {<SEARCH API>} from "@btd/JSSearchAlgorithms";
<SEARCH API>(<[Integer Array]>);

Example:

import {sequentialSearch} from '@btd/JSSearchAlgorithms';
sequentialSearch([1, 2], 1);
````
