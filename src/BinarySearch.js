/**
 * Internal binary search function.
 * @param {Array} inputArray input array
 * @param {Integer} searchNumber number being searched
 * @param {Integer} startPoint starting index
 * @param {Integer} endPoint end index
 * @return {Integer} index if present, -1 if not
 */
function _binarySearchFunction(inputArray, searchNumber, startPoint, endPoint) {
    let pivot = (endPoint - startPoint) === 1 ? startPoint :
        Math.ceil(startPoint + (endPoint - startPoint) / 2);
    let pivotElement = inputArray[pivot];
    if (pivotElement === searchNumber) {
        return pivot;
    } else if (pivot === startPoint || pivot === endPoint) {
        return -1;
    } else if (pivotElement > searchNumber) {
        // Search start to pivot
        return _binarySearchFunction(inputArray, searchNumber,
            startPoint, pivot);
    } else {
        // Search pivot to end
        return _binarySearchFunction(inputArray, searchNumber,
            pivot, endPoint);
    }
}

/**
 * Binary Search Function
 * @param {Array} inputArray input array
 * @param {Number} searchNumber number being searched
 * @return {Number} index index of the found number. -1 if not found
 */
export default function binarySearch(inputArray, searchNumber) {
    if (inputArray.length === 0) {
        return -1;
    }
    return _binarySearchFunction(inputArray, searchNumber,
        0, inputArray.length);
}
