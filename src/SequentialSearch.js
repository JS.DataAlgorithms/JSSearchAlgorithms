/**
 * Sequential Search Function
 *
 * Run through each element starting from 0 index,
 * compare the same to the search element, if found, stop.
 *
 * @param {Array} inputArray input array
 * @param {Number} searchNumber number being searched
 * @return {Number} index index of the found number. -1 if not found
 */
export default function sequentialSearch(inputArray, searchNumber) {
    for (const [index, element] of inputArray.entries()) {
        if (element === searchNumber) {
            return index;
        }
    }
    return -1;
}
