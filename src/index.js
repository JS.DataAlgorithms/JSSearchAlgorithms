import binarySearch from './BinarySearch';
import sequentialSearch from './SequentialSearch';

export {binarySearch, sequentialSearch};
