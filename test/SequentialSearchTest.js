import {expect} from 'chai';
import sequentialSearch from '../src/SequentialSearch';

describe('Sequential Search', function() {
    it('should search and return index', function() {
        expect(sequentialSearch([1, 2], 1)).to.equal(0);
    });
    it('should search and return -1', function() {
        expect(sequentialSearch([1, 2], 3)).to.equal(-1);
    });
    it('should search and return index - large data set', function() {
        let randomArray = Array.from({length: 1000},
                () => Math.floor(Math.random() * 1000));
        randomArray.push(1000);
        expect(sequentialSearch(randomArray, 1000)).to.not.equal(-1);
    });
    it('should search and return -1 - large data set', function() {
        const randomArray = Array.from({length: 1000},
                () => Math.floor(Math.random() * 1000));
        expect(sequentialSearch(randomArray, -1)).to.equal(-1);
    });
});
